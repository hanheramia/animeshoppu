import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DefaultRoutingModule } from './default-routing.module';


@NgModule({
  imports: [
    CommonModule,
    DefaultRoutingModule
  ]
})
export class DefaultModule { }
