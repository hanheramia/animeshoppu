import { Component, Input } from '@angular/core';

@Component({
  selector: 'best-seller',
  templateUrl: './best-seller.component.html',
  styleUrls: ['./best-seller.component.scss']
})
export class BestSellerComponent {

  @Input()
  public banners: string[]

}
