import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarouselModule } from 'primeng/carousel';
import { BestSellerComponent } from './best-seller.component';


@NgModule({
  declarations: [
    BestSellerComponent
  ],
  imports: [
    CommonModule,
    CarouselModule
  ],
  exports: [
    BestSellerComponent
  ]
})
export class BestSellerModule { }
