import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'carousel-display',
  templateUrl: './carousel-display.component.html',
  styleUrls: ['./carousel-display.component.scss']
})
export class CarouselDisplayComponent {

  @Input()
  public banners: string[]

}
