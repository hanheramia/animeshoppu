import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarouselModule } from 'primeng/carousel';
import { CarouselDisplayComponent } from './carousel-display.component';

@NgModule({
  declarations: [
    CarouselDisplayComponent
  ],
  imports: [
    CommonModule,
    CarouselModule
  ],
  exports: [
    CarouselDisplayComponent
  ]
})
export class CarouselDisplayModule { }
