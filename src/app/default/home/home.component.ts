import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  public homeBanners: string[] = [
    "banner_first.png",
    "banner_second.png",
    "banner_third.png"
  ]

  public bestSellers: string[] = [
    "best_seller1.png",
    "best_seller2.png",
    "best_seller3.png"
  ]

}
