import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CarouselDisplayModule } from './carousel-display/carousel-display.module';
import { SearchBarModule } from 'src/app/shared/components/searchbar/searchbar.module';
import { BestSellerModule } from './best-seller/best-seller.module';
import { AboutUsModule } from './about-us/about-us.module';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CarouselDisplayModule,
    SearchBarModule,
    BestSellerModule,
    AboutUsModule
  ]
})
export class HomeModule { }
