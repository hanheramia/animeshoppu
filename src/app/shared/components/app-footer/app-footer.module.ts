import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppFooterRoutingModule } from './app-footer-routing.module';
import { AppFooterComponent } from './app-footer.component';


@NgModule({
  declarations: [
    AppFooterComponent
  ],
  imports: [
    CommonModule,
    AppFooterRoutingModule
  ],
  exports: [
    AppFooterComponent,
  ]
})
export class AppFooterModule { } 
