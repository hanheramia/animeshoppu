import { ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
})
export class AppHeaderComponent {

  public options: SelectItem[] = [];
  public selectedOption: string;

  constructor(private router: Router,
    private cd: ChangeDetectorRef) {
    this.options = [
      { label: 'Home   ', value: 'home' },
      { label: 'Products', value: 'products' },
      { label: 'Updates', value: 'updates' },
      { label: 'FAQ    ', value: 'faq' },
      { label: 'Contact ', value: 'contact' }
    ];
  }

  public onSelect(event) {
    if (event.option) {
      const selectedOption: SelectItem = event.option;
      switch (selectedOption.value) {
        case "home": {
          this.router.navigateByUrl('/home');
          break;
        }
        case "products": {
          this.router.navigateByUrl('/products');
          break;
        }
        case "updates": {
          // return when already have updates url
          this.router.navigateByUrl('/products');
          // this.router.navigateByUrl('/updates');
          break;
        }
        case "faq": {
          // return when already have faq url
          // this.router.navigateByUrl('/faq');
          this.router.navigateByUrl('/products');
          break;
        }
        case "contact": {
          this.router.navigateByUrl('/contactus');
          break;
        }
        default: {
          this.router.navigateByUrl('/home');
          break;
        }
      }
    }

  }
}
