import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectButtonModule } from 'primeng/selectbutton';
import { FormsModule } from '@angular/forms';
import { SearchBarComponent } from './searchbar.component';


@NgModule({
  declarations: [
    SearchBarComponent,
  ],
  imports: [
    CommonModule,
    SelectButtonModule,
    FormsModule
  ],
  exports: [
    SearchBarComponent,
  ]
})
export class SearchBarModule { }
